using System;
using Tools;
using UnityEngine;

namespace Mechanics
{
    public class InputManager : MonoBehaviour
    {
        [SerializeField] private GameObject cueStick;
        [SerializeField] private float cueYOffset;

        [SerializeField] private GameObject redBal;

        private Camera _camera;
        private float _distance;

        public static Vector3 SelectedPoint { get; set; }

        private void Start()
        {
            _camera = Camera.main;
        }

        private void Update()
        {
            UpdateSelection();
        }

        private void UpdateSelection()
        {
            if (!_camera) return;

            if (Input.GetMouseButtonDown(0))
            {
                Ray ray = _camera.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out var raycastHit, 40f,
                    LayerMask.GetMask(Layers.Board)))
                {
                    Debug.Log(SelectedPoint);
                    SelectedPoint = new Vector3(raycastHit.point.x, cueYOffset, raycastHit.point.z);
                    Vector3 balPos = redBal.transform.position;

                    Vector3 newCuePos = new Vector3(balPos.x, cueYOffset, balPos.z);
                    cueStick.transform.position = newCuePos;
                    cueStick.transform.LookAt(SelectedPoint);

                    Vector3 point = Extensions.LerpByDistance(newCuePos, SelectedPoint, -0.15f);
                    cueStick.transform.position = point;
                    _distance = Vector3.Distance(newCuePos, SelectedPoint);
                }
                // else
                    // SelectedPoint = Vector2.one * -1f;
            }

            if (Input.GetMouseButton(0))
            {
                
                Vector3 point = Extensions.LerpByDistance(cueStick.transform.position, SelectedPoint, _distance);
                cueStick.transform.position = point;
                // cueStick.transform.position = 
            }
        }
    }
}