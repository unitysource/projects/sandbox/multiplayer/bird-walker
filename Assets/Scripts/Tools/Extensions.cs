﻿using UnityEngine;

namespace Tools
{
    public static class Extensions
    {
        public static Vector2 Get2DPosition(Vector3 position)
        {
            return new Vector2(position.x, position.z);
        }
        
        public static Vector2 Get3DPosition(Vector2 position, float yOffset)
        {
            return new Vector3(position.x, yOffset, position.y);
        }

        public static float AngleBetweenTwoPoints(Vector3 a, Vector3 b) => 
            Mathf.Atan2(a.y - b.y, a.x - b.x) * Mathf.Rad2Deg;
        
        /// <summary>
        /// Get point along line
        /// </summary>
        /// <param name="A">first point</param>
        /// <param name="B">second point</param>
        /// <param name="x">distance to the point A</param>
        /// <returns></returns>
        public static Vector3 LerpByDistance(Vector3 A, Vector3 B, float x) => 
            x * Vector3.Normalize(B - A) + A;
        
        /// <summary>
        /// Get point by 90 deg along line (https://answers.unity.com/questions/1458726/find-point-along-line.html)
        /// </summary>
        /// <param name="A"></param>
        /// <param name="B"></param>
        /// <param name="point"></param>
        /// <returns></returns>
        public static Vector3 GetPositionOnSegment( Vector3 A, Vector3 B, Vector3 point  )
        {
            Vector3 projection = Vector3.Project( point - A, B - A ) ;
            return projection + A ;
        }
    }
}